package com.example.foodapp.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.example.foodapp.R;
import com.example.foodapp.adapter.ProdutoAdapter;
import com.example.foodapp.database.Database;
import com.example.foodapp.object.Produtos;

import java.util.ArrayList;
import java.util.List;

public class AcessoApp extends AppCompatActivity {

    private ViewHolder mViewHolder = new ViewHolder();
    ArrayAdapter<Produtos> adapter;
    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acesso_app);

        Intent i = getIntent();
        Bundle extras = i.getExtras();
        final int category_id = extras != null ? extras.getInt("categoria_id", 0) : 0;

        List<Produtos> listaProdutos = new ArrayList<>();

        db = new Database(this);


        Cursor c = db.Select_PesquisarProduto("", category_id);
        c.moveToFirst();

        if(c.getCount()>0){
            do {
                Integer id_produto = c.getInt(c.getColumnIndex("id_produto"));
                String nome_produto = c.getString(c.getColumnIndex("nome_produto"));
                String gama_preco = c.getString(c.getColumnIndex("gama_preco"));
                Integer foto = c.getInt(c.getColumnIndex("foto"));

                listaProdutos.add(new Produtos(id_produto, nome_produto, gama_preco, foto));

            }while (c.moveToNext());
        }

        mViewHolder.rv_produtos = (RecyclerView)findViewById(R.id.rv_produtos);
        mViewHolder.et_pesquisa = (EditText)findViewById(R.id.et_pesquisa);
        mViewHolder.bt_filtrar = (ImageButton) findViewById(R.id.bt_filtrar);

        mViewHolder.bt_filtrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AcessoApp.this, Filtragem.class));
            }
        });


        mViewHolder.et_pesquisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                List<Produtos> pesquisaProdutos = new ArrayList<>();

                String nomeproduto = charSequence.toString();
                Cursor c = db.Select_PesquisarProduto(nomeproduto, category_id);
                c.moveToFirst();

                if(c.getCount()>0){
                    do {
                        Integer id_produto = c.getInt(c.getColumnIndex("id_produto"));
                        String nome_produto = c.getString(c.getColumnIndex("nome_produto"));
                        String gama_preco = c.getString(c.getColumnIndex("gama_preco"));
                        Integer foto = c.getInt(c.getColumnIndex("foto"));

                        pesquisaProdutos.add(new Produtos(id_produto, nome_produto, gama_preco, foto));

                    }while (c.moveToNext());
                }
                ProdutoAdapter produtoAdapter = new ProdutoAdapter(pesquisaProdutos);
                mViewHolder.rv_produtos.setAdapter(produtoAdapter);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        ProdutoAdapter produtoAdapter = new ProdutoAdapter(listaProdutos);
        mViewHolder.rv_produtos.setAdapter(produtoAdapter);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mViewHolder.rv_produtos.setLayoutManager(new GridLayoutManager(this, 3));


    }

    private static class ViewHolder{
        RecyclerView rv_produtos;
        EditText et_pesquisa;
        ImageButton bt_filtrar;
    }
}
