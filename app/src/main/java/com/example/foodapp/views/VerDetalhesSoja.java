package com.example.foodapp.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;

import com.example.foodapp.R;
import com.example.foodapp.adapter.DetalheListAdapter;
import com.example.foodapp.adapter.ProdutoAdapter;
import com.example.foodapp.database.Database;
import com.example.foodapp.object.Detalhes;
import com.example.foodapp.object.Produtos;

import java.util.ArrayList;
import java.util.List;

public class VerDetalhesSoja extends AppCompatActivity {

    private ViewHolder mViewHolder = new ViewHolder();
    ArrayAdapter<Detalhes> adapter;
    Database db;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_detalhes_soja);
        mViewHolder.rv_detalhes=(RecyclerView)findViewById(R.id.rv_detalhes);

        List<Detalhes> listaDetalhes = new ArrayList<>();

        db = new Database(this);
i=getIntent();

        Cursor c = db.Select_DetalhesById(i.getExtras().getInt("id_produto"));
        c.moveToFirst();
        if(c.getCount()>0){
            do {
                int id_detalhe = c.getInt(c.getColumnIndex("id_detalhe"));
                String ingredientes_produto = c.getString(c.getColumnIndex("ingredientes_produto"));
                String texto_receita = c.getString(c.getColumnIndex("texto_receita"));
                String nome_receita = c.getString(c.getColumnIndex("nome_receita"));
                String ingredientes_receita = c.getString(c.getColumnIndex("ingredientes_receita"));
                String valor_nutricional = c.getString(c.getColumnIndex("valor_nutricional"));
                Integer foto_receita = c.getInt(c.getColumnIndex("foto_receita"));
                Integer id_produto = c.getInt(c.getColumnIndex("id_produto"));
                String nome_produto="";
                Cursor c1 = db.Select_NomeByIdProduto(id_produto);
                c1.moveToFirst();
                if(c1.getCount()>0) {
                     nome_produto = c1.getString(c1.getColumnIndex("nome_produto"));

                }while (c1.moveToNext());


                listaDetalhes.add(new Detalhes(id_detalhe, ingredientes_produto, texto_receita, nome_receita, ingredientes_receita, valor_nutricional, foto_receita, id_produto, nome_produto));

            }while (c.moveToNext());
        }



        DetalheListAdapter detalheListAdapter = new DetalheListAdapter(listaDetalhes);
        mViewHolder.rv_detalhes.setAdapter(detalheListAdapter);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mViewHolder.rv_detalhes.setLayoutManager(linearLayoutManager);


    }

    private static class ViewHolder{
        RecyclerView rv_detalhes;
    }
}
