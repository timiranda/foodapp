package com.example.foodapp.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.foodapp.database.Database;
import com.example.foodapp.R;

public class CriarUtilizador extends AppCompatActivity {

    private ViewHolder mViewHolder = new ViewHolder();
    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criar_utilizador);

        db = new Database(this);

        mViewHolder.et_username = (EditText)findViewById(R.id.et_username);
        mViewHolder.et_password = (EditText)findViewById(R.id.et_password);
        mViewHolder.bt_criar = (ImageButton) findViewById(R.id.bt_criar);


        mViewHolder.bt_criar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = mViewHolder.et_username.getText().toString();
                String password = mViewHolder.et_password.getText().toString();

                long res = db.InserirUtilizador(username, password);

                Toast.makeText(CriarUtilizador.this, "Utilizador registado com sucesso!",
                        Toast.LENGTH_SHORT).show();
                Intent i = new Intent(CriarUtilizador.this, EcraLogin.class);
                startActivity(i);

            }
        });
    }

    private static class ViewHolder{
        EditText et_username;
        EditText et_password;
        ImageButton bt_criar;
    }
}
