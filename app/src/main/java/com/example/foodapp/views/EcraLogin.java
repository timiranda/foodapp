package com.example.foodapp.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.foodapp.R;
import com.example.foodapp.database.Database;
import com.example.foodapp.object.Utilizadores;

import java.util.ArrayList;
import java.util.List;

public class EcraLogin extends AppCompatActivity {

    private ViewHolder mViewHolder = new ViewHolder();
    Database db;
    SQLiteDatabase mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecra_login);

        db = new Database(this);
        mDatabase = db.getWritableDatabase();
        final List<Utilizadores> listaUtilizadores = new ArrayList<>();


        mViewHolder.et_username = (EditText)findViewById(R.id.et_username);
        mViewHolder.et_password = (EditText)findViewById(R.id.et_password);
        mViewHolder.bt_login = (ImageButton) findViewById(R.id.bt_login);
        mViewHolder.bt_criar = (ImageButton) findViewById(R.id.bt_criar);


        /* if(db.retornaNumUtilizadores() == 0)
        {
            Toast tostada = Toast.makeText(this,"Não existem utilizadores definidos. Crie um.",Toast.LENGTH_LONG);
            tostada.show();
            Intent i = new Intent(EcraLogin.this, CriarUtilizador.class);
            startActivity(i);
        } */

        mViewHolder.bt_criar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(EcraLogin.this, CriarUtilizador.class);
                startActivity(i);
            }
        });

        mViewHolder.bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String usernamelog = mViewHolder.et_username.getText().toString();
                String passwordlog = mViewHolder.et_password.getText().toString();

                final Cursor c = db.Select_All_Utilizador();
                c.moveToFirst();

                if (c.getCount() > 0) {
                    do {
                        String username = c.getString(c.getColumnIndex("username")).toString();
                        String password = c.getString(c.getColumnIndex("password")).toString();
                        listaUtilizadores.add(new Utilizadores(username, password));

                        if (usernamelog.equals(username) && passwordlog.equals(password)) {
                            Toast.makeText(EcraLogin.this, "Login válido", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(EcraLogin.this, AcessoApp.class);
                            startActivity(i);
                        }
                        else {
                            Toast.makeText(EcraLogin.this, "Login inválido", Toast.LENGTH_SHORT).show();
                            mViewHolder.et_username.getText().clear();
                            mViewHolder.et_password.getText().clear();
                        }
                    } while (c.moveToNext());

                }

            }
        });

    }

    private static class ViewHolder{
        EditText et_username;
        EditText et_password;
        ImageButton bt_login;
        ImageButton bt_criar;
    }
}
