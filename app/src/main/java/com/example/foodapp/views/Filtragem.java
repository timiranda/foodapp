package com.example.foodapp.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.example.foodapp.R;
import com.example.foodapp.database.Database;
import com.example.foodapp.object.Categorias;
import com.example.foodapp.object.Detalhes;
import com.example.foodapp.object.Produtos;

import java.util.ArrayList;
import java.util.List;

public class Filtragem extends AppCompatActivity {

    private ViewHolder mViewHolder = new ViewHolder();
    ArrayList<Categorias> listaCategorias;
    ArrayAdapter<Categorias> adapter;
    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtragem);

        listaCategorias = new ArrayList<Categorias>();

        db = new Database(this);

        Cursor c = db.Select_All_Categorias();

        try {
            while (c.moveToNext()) {
                int id_categoria = c.getInt(c.getColumnIndex("id_categoria"));
                String nome_categoria = c.getString(c.getColumnIndex("nome_categoria"));
                listaCategorias.add(new Categorias(
                        id_categoria,
                        nome_categoria
                ));
            }
        } finally {
            c.close();
        }

        mViewHolder.lv_filtragem=(ListView)findViewById(R.id.lv_filtragem);
        mViewHolder.bt_remover_filtros=(ImageButton) findViewById(R.id.bt_remover_filtros);
        mViewHolder.bt_voltar=(ImageButton)findViewById(R.id.bt_voltar);

        adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, listaCategorias);

        mViewHolder.lv_filtragem.setAdapter(adapter);

        mViewHolder.lv_filtragem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int categoria_id = listaCategorias.get(i).getId_categoria();
                Intent i1 = new Intent(Filtragem.this, AcessoApp.class);
                i1.putExtra("categoria_id", categoria_id);
                startActivity(i1);
            }
        });

        mViewHolder.bt_remover_filtros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i1 = new Intent(Filtragem.this, AcessoApp.class);
                i1.putExtra("categoria_id", 0);
                startActivity(i1);
            }
        });

        mViewHolder.bt_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Filtragem.this, AcessoApp.class);
                startActivity(i);
            }
        });
    }

    private static class ViewHolder{
        ListView lv_filtragem;
        ImageButton bt_remover_filtros;
        ImageButton bt_voltar;
    }
}
