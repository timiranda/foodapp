package com.example.foodapp.object;

public class Detalhes {
    private int id_detalhe;
    private String ingredientes_produto;
    private String texto_receita;
    private String nome_receita;
    private String ingredientes_receita;
    private String valor_nutricional;
    private int foto_receita;
    private int id_produto;
    private String nome_produto;

    public Detalhes(int id_detalhe, String ingredientes_produto, String texto_receita, String nome_receita, String ingredientes_receita, String valor_nutricional, int foto_receita, int id_produto, String nome_produto) {
        this.setId_detalhe(id_detalhe);
        this.setIngredientes_produto(ingredientes_produto);
        this.setTexto_receita(texto_receita);
        this.setNome_receita(nome_receita);
        this.setIngredientes_receita(ingredientes_receita);
        this.setValor_nutricional(valor_nutricional);
        this.setFoto_receita(foto_receita);
        this.setId_produto(id_produto);
        this.setNome_produto(nome_produto);
    }


    public int getId_detalhe() {
        return id_detalhe;
    }

    public void setId_detalhe(int id_detalhe) {
        this.id_detalhe = id_detalhe;
    }

    public String getIngredientes_produto() {
        return ingredientes_produto;
    }

    public void setIngredientes_produto(String ingredientes_produto) {
        this.ingredientes_produto = ingredientes_produto;
    }

    public String getNome_receita() {
        return nome_receita;
    }

    public void setNome_receita(String nome_receita) {
        this.nome_receita = nome_receita;
    }

    public String getIngredientes_receita() {
        return ingredientes_receita;
    }

    public void setIngredientes_receita(String ingredientes_receita) {
        this.ingredientes_receita = ingredientes_receita;
    }

    public String getTexto_receita() {
        return texto_receita;
    }

    public void setTexto_receita(String texto_receita) {
        this.texto_receita = texto_receita;
    }

    public String getValor_nutricional() {
        return valor_nutricional;
    }

    public void setValor_nutricional(String valor_nutricional) {
        this.valor_nutricional = valor_nutricional;
    }

    public int getFoto_receita() {
        return foto_receita;
    }

    public void setFoto_receita(int foto_receita) {
        this.foto_receita = foto_receita;
    }

    public int getId_produto() {
        return id_produto;
    }

    public void setId_produto(int id_produto) {
        this.id_produto = id_produto;
    }

    public String getNome_produto() {
        return nome_produto;
    }

    public void setNome_produto(String nome_produto) {
        this.nome_produto = nome_produto;
    }

}