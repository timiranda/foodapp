package com.example.foodapp.object;

public class Produtos {
    private int id_produto;
    private String nome_produto;
    private String gama_preco;
    private int foto;

    public Produtos(int id_produto, String nome_produto, String gama_preco, int foto) {
        this.setId_produto(id_produto);
        this.setNome_produto(nome_produto);
        this.setGama_preco(gama_preco);
        this.setFoto(foto);
    }

    public int getId_produto() {
        return id_produto;
    }

    public void setId_produto(int id_produto) {
        this.id_produto = id_produto;
    }

    public String getNome_produto() {
        return nome_produto;
    }

    public void setNome_produto(String nome_produto) {
        this.nome_produto = nome_produto;
    }

    public String getGama_preco() {
        return gama_preco;
    }

    public void setGama_preco(String gama_preco) {
        this.gama_preco = gama_preco;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }
}
