package com.example.foodapp.viewholder;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodapp.R;
import com.example.foodapp.object.Produtos;

public class ProdutoViewHolder extends RecyclerView.ViewHolder {

    private ImageView iv_imgproduto;
    private TextView tv_nomeproduto;
    private TextView tv_precoproduto;
    //private TextView tv_idproduto;
    private Button bt_vermais;

    public ProdutoViewHolder(@NonNull View itemView) {
        super(itemView);
        iv_imgproduto=(ImageView)itemView.findViewById(R.id.iv_imgproduto);
        tv_nomeproduto=(TextView)itemView.findViewById(R.id.tv_nomeproduto);
        tv_precoproduto=(TextView)itemView.findViewById(R.id.tv_precoproduto);
        //tv_idproduto=(TextView)itemView.findViewById(R.id.tv_idproduto);
        bt_vermais=(Button)itemView.findViewById(R.id.bt_vermais);
    }

    public void bindData(Produtos produtos){
        iv_imgproduto.setImageResource(produtos.getFoto());
        tv_nomeproduto.setText(produtos.getNome_produto());
        tv_precoproduto.setText(produtos.getGama_preco());
        //tv_idproduto.setText(String.valueOf(produtos.getId_produto()));
    }
}
