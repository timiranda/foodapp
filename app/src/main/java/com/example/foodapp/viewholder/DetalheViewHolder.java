package com.example.foodapp.viewholder;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodapp.R;
import com.example.foodapp.object.Detalhes;

public class DetalheViewHolder extends RecyclerView.ViewHolder {

    private TextView tv_nomeproduto;
    private TextView tv_ingredientes;
    private TextView tv_valornutricional;
    private TextView tv_nomereceita;
    private TextView tv_ingredientesreceita;
    private TextView tv_receita;
    private ImageView iv_imgreceita;
    private Button bt_voltar;

    public DetalheViewHolder(@NonNull View itemView) {
        super(itemView);
        tv_nomeproduto=(TextView)itemView.findViewById(R.id.tv_nomeproduto);
        tv_ingredientes=(TextView)itemView.findViewById(R.id.tv_ingredientes);
        tv_valornutricional=(TextView)itemView.findViewById(R.id.tv_valornutricional);
        tv_nomereceita=(TextView)itemView.findViewById(R.id.tv_nomereceita);
        tv_ingredientesreceita=(TextView)itemView.findViewById(R.id.tv_ingredientesreceita);
        tv_receita=(TextView)itemView.findViewById(R.id.tv_receita);
        iv_imgreceita=(ImageView)itemView.findViewById(R.id.iv_imgreceita);
        bt_voltar=(Button)itemView.findViewById(R.id.bt_voltar);

    }

    public void bindData(Detalhes detalhes) {
        tv_nomeproduto.setText(detalhes.getNome_produto());
        tv_ingredientes.setText(detalhes.getIngredientes_produto());
        tv_valornutricional.setText(detalhes.getValor_nutricional());
        tv_receita.setText(detalhes.getTexto_receita());
        tv_nomereceita.setText(detalhes.getNome_receita());
        tv_ingredientesreceita.setText(detalhes.getIngredientes_receita());
        iv_imgreceita.setImageResource(detalhes.getFoto_receita());
    }
}
