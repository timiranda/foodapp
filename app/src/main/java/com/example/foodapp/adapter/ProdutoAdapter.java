package com.example.foodapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodapp.R;
import com.example.foodapp.object.Produtos;
import com.example.foodapp.viewholder.ProdutoViewHolder;
import com.example.foodapp.views.VerDetalhesSoja;

import java.util.List;

public class ProdutoAdapter extends RecyclerView.Adapter<ProdutoViewHolder> {

    private List<Produtos> listaProdutos;
    private Button bt_vermais;

    public ProdutoAdapter(List<Produtos> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }

    @NonNull
    @Override
    public ProdutoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View produtoView = inflater.inflate(R.layout.row_produto_list, parent, false);
        return new ProdutoViewHolder(produtoView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProdutoViewHolder holder, final int position) {
        Produtos produtos = listaProdutos.get(position);
        holder.bindData(produtos);
        bt_vermais = holder.itemView.findViewById(R.id.bt_vermais);
        bt_vermais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent i = new Intent(view.getContext(), VerDetalhesSoja.class);
                i.putExtra("id_produto",listaProdutos.get(position).getId_produto());
                view.getContext().startActivity(i);
                /*
                if (position == 0) {
                Intent i = new Intent(view.getContext(), VerDetalhesSoja.class);
                view.getContext().startActivity(i);
                } else if (position == 1) {
                    Intent i = new Intent(view.getContext(), VerDetalhesTortellini.class);
                    view.getContext().startActivity(i);
                }*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaProdutos.size();
    }
}
