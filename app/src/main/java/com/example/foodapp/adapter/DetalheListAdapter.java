package com.example.foodapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.foodapp.R;
import com.example.foodapp.object.Detalhes;
import com.example.foodapp.object.Produtos;
import com.example.foodapp.viewholder.DetalheViewHolder;
import com.example.foodapp.viewholder.ProdutoViewHolder;
import com.example.foodapp.views.AcessoApp;

import java.util.List;

public class DetalheListAdapter extends RecyclerView.Adapter<DetalheViewHolder> {

    private List<Detalhes> listaDetalhes;
    Button bt_voltar;

    public DetalheListAdapter(List<Detalhes> listaDetalhes) {
        this.listaDetalhes = listaDetalhes;}

    @NonNull
    @Override
    public DetalheViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View detalheView = inflater.inflate(R.layout.row_detalhe_list, parent, false);
        return new DetalheViewHolder(detalheView);
    }

    @Override
    public void onBindViewHolder(@NonNull DetalheViewHolder holder, final int position) {
        Detalhes detalhes = listaDetalhes.get(position);
        holder.bindData(detalhes);
        bt_voltar = holder.itemView.findViewById(R.id.bt_voltar);
        bt_voltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), AcessoApp.class);
                view.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaDetalhes.size();
    }
}
