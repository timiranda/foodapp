package com.example.foodapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.foodapp.R;

public class Database extends SQLiteOpenHelper {

    private static int version = 1;
    private static String nameDB = "Database.db";

    String[] sql = {
            "CREATE TABLE Utilizador(username TEXT PRIMARY KEY," +
                    "password TEXT);",
            "CREATE TABLE Categorias(id_categoria INTEGER PRIMARY KEY," +
                    "nome_categoria TEXT);",
            "CREATE TABLE Produtos(id_produto INTEGER PRIMARY KEY," +
                    "nome_produto TEXT," +
                    "gama_preco TEXT," +
                    "id_categoria INTEGER," +
                    "foto INTEGER," +
                    "FOREIGN KEY(id_categoria) REFERENCES Categorias(id_categoria));",
                    "CREATE TABLE DetalhesProduto(id_detalhe INTEGER PRIMARY KEY," +
                    "ingredientes_produto TEXT," +
                    "nome_receita TEXT," +
                    "ingredientes_receita TEXT," +
                    "texto_receita TEXT," +
                    "valor_nutricional TEXT," +
                    "foto_receita INTEGER, id_produto INTEGER, " +
                    "FOREIGN KEY(id_produto) REFERENCES Produtos(id_produto));",
                    "INSERT INTO Utilizador(username, password) VALUES ('teresa','passe');",
                    "INSERT INTO Categorias(id_categoria, nome_categoria) VALUES (1,'Molhos');",
                    "INSERT INTO Categorias(id_categoria, nome_categoria) VALUES (2,'Massas');",
                    "INSERT INTO Produtos(id_produto, nome_produto, gama_preco, id_categoria, foto) VALUES (1,'Molho de Soja','2-4€',1," + R.drawable.molhosoja + ");",
                    "INSERT INTO Produtos(id_produto, nome_produto, gama_preco, id_categoria, foto) VALUES (2,'Tortellini','2-5€',2," + R.drawable.tortellini + ");",
                    "INSERT INTO Produtos(id_produto, nome_produto, gama_preco, id_categoria, foto) VALUES (3,'Hóstias de Camarão','1-3€',null," + R.drawable.hostiascamarao + ");",
                    "INSERT INTO Produtos(id_produto, nome_produto, gama_preco, id_categoria, foto) VALUES (4,'Curry Spice Paste','3-6€',1," + R.drawable.curry + ");",
                    "INSERT INTO Produtos(id_produto, nome_produto, gama_preco, id_categoria, foto) VALUES (5,'Massa de Arroz','2-4€',2," + R.drawable.massaarroz + ");",
                    "INSERT INTO Produtos(id_produto, nome_produto, gama_preco, id_categoria, foto) VALUES (6,'Molho Barbecue','7-10€',1," + R.drawable.bbqsauce + ");",
                    "INSERT INTO Produtos(id_produto, nome_produto, gama_preco, id_categoria, foto) VALUES (7,'Cogumelos desidratados','8-15€',null," + R.drawable.driedmushrooms + ");",
                    "INSERT INTO Produtos(id_produto, nome_produto, gama_preco, id_categoria, foto) VALUES (8,'Carne de caranguejo','2-5€',null," + R.drawable.crabmeat + ");",
                    "INSERT INTO DetalhesProduto(id_detalhe, ingredientes_produto, nome_receita, ingredientes_receita, texto_receita, valor_nutricional, foto_receita, id_produto) VALUES (1,'Ingredientes: Recheio 51%: fiambre 21% (carne de porco, sal, dextrose, aromas), ricota 15% (soro de leite, leite), queijo magro 10,5%, fibras vegetais, natas, oleos vegetais, gorduras vegetais, amido, pao ralado (farinha de trigo mole, sal, fermento), aromas naturais, soro de leite em po, queijo ralado 3%, manteiga, sal. Massa 49%: farinha de trigo mole, farelo de trigo rijo, ovo 20%.'," +
                            "'Macarrão Wok com com cogumelos e molho de soja'," +
                            "'160 gr Macarrão Wok Milaneza\r\n" +
                            "100 gr cogumelos pleurotus\r\n" +
                            "100 gr cogumelos shitaki\r\n" +
                            "100 gr cogumelos enoki\r\n" +
                            "40 gr cebola\r\n" +
                            "1 dente de alho\r\n" +
                            "2 cl molho de soja\r\n" +
                            "300 ml água ou caldo de legumes\r\n" +
                            "q.b. salsa fresca\r\n" +
                            "q.b. azeite\r\n" +
                            "q.b. sal'," +
                            "'Aquecer o azeite no wok, adicionar a cebola e o alho picado e deixar refogar." +
                            "Adicionar os enokis previamente escolhidos e lavados inteiros, os pleurotus em tiras e os shitaki aos cubos, temperar com sal. Colocar o molho de soja e mexer bem.\n" +
                            "Adicionar a água e o macarrão wok Milaneza. Deixar cozinhar durante 4 minutos. No final acrescentar a salsa picada e retificar temperos.'," +
                            "'Valores nutricionais médios por 100ml: (Energia: 761 kJ / 179 kcal; Lípidos: <0.10 g; Hidratos de carbono: 41 g; Proteínas: 3.80 g) ', " + R.drawable.macarrao + ", 1);",
            "INSERT INTO DetalhesProduto(id_detalhe, ingredientes_produto, nome_receita, ingredientes_receita, texto_receita, valor_nutricional, foto_receita, id_produto) VALUES (2,'Ingredientes: Água, Xarope de Frutose, Glicose, Açúcar, Molho de SOJA (Água, SOJA, TRIGO, Sal), Melaço, Sal, Extrato de Levedura, Corante (Caramelo), Ervas e Extratos de Especiarias, Acidulante (Ácido Láctico).'," +
                    "'Tortellini com cogumelos'," +
                    "'500 g de tortellini de carne\r\n" +
                    "1 Cebola\r\n" +
                    "2 dentes de Alho\r\n" +
                    "2 tomates\r\n" +
                    "50 ml de Azeite\r\n" +
                    "100 g de cogumelos\r\n" +
                    "1 c. (sopa) de polpa de tomate\r\n" +
                    "2 dl de Vinho Branco\r\n" +
                    "4 c. (sopa) de queijo parmesão ralado\r\n" +
                    "sal q.b.\r\n" +
                    "pimenta q.b.\r\n" +
                    "manjericão q.b.'," +
                    "'Coza os tortellini em água fervente temperada com sal, de acordo com as instruções da embalagem; retire, escorra, passe por água fria e reserve. Pique a cebola, os dentes de alho e lamine os cogumelos. Pele o tomate, com uma faca bem afiada e corte-o em pedacinhos.\r\n" +
                    "Numa frigideira, verta o azeite e refogue as cebolas e os dentes de alho picados. Junte os cogumelos e deixe saltearem. Adicione os pedaços de tomate e a polpa de tomate. Regue com o vinho e deixe apurar.\r\n" +
                    "Coloque a massa num prato de servir, regue com o molho de tomate e polvilhe com o queijo parmesão. Sirva de imediato.'," +
                    "'Valores nutricionais médios por 100g: Não Preparado: Energia: 1134.70 kJ / 268.60 kcal; Lípidos: 4.70 g, Dos quais ácidos gordos saturados: 1.96 g; Hidratos de carbono: 45.00 g, Dos quais açúcares: 5.10 g; Fibras: 1.70 g; Proteínas: 10.70 g; Sal: 0.90 g ', " + R.drawable.tortellinireceita + ", 2);",
            "INSERT INTO DetalhesProduto(id_detalhe, ingredientes_produto, nome_receita, ingredientes_receita, texto_receita, valor_nutricional, foto_receita, id_produto) VALUES (3,'Ingredientes: farinha de tapioca, gordura vegetal de palma, CARNE DE CAMARÃO (15.4%), açúcar, OVO, sal, antioxidante E320, antioxidante E310, antioxidante E330 Contém Crustáceos, Ovos. Pode conter vestígios de Aipo, Peixe, Leite.'," +
                    "'Hóstias de camarão'," +
                    "'1 embalagem de Prawn Crackers\r\n" +
                    "Óleo (de preferência de amendoim)'," +
                    "'Coloque uma quantidade considerável de óleo de amendoim no wok e aqueça-o (mais ou menos 170º) - também pode fazer na fritadeira.\r\n" +
                    "Insira as rodelas de prawn crackers no óleo quente e quando elas vierem à superfície (já crescidas e brancas) retire.\r\n" +
                    "Coloque sobe papel absorvente e sirva em seguida.'," +
                    "'Valores nutricionais médios por 100g: Preparado: Energia: 2191.00 kJ / 523.00 kcal; Lípidos: 23.00 g, Dos quais ácidos gordos saturados: 13.80 g; Hidratos de carbono: 73.40 g, Dos quais açúcares: 5.70 g; Fibras: 9.00 g; Proteínas: 1.20 g; Sal: 1.50 g', " + R.drawable.hostiasreceita + ", 3);",
            "INSERT INTO DetalhesProduto(id_detalhe, ingredientes_produto, nome_receita, ingredientes_receita, texto_receita, valor_nutricional, foto_receita, id_produto) VALUES (4,'Ingredientes: Especiarias moídas 51% (Coentros, açafrão, mostarda, fenogrego, cominhos, farinha de grão, piripiri (2%), semente papoila) água, óleo de girasol, sal, cebola, gengibre, coentros, piripiri, alho, tamarindo, sumo limao, regulador de acidez: Ácido acético E260, Conservantes: Benzoato de sódio E-211, Corante: extracto de paprika E-160C.'," +
                    "'Caril de frango Thai'," +
                    "'500g de Peito de frango\r\n" +
                    "2 latas de Leite de coco\r\n" +
                    "Azeite q.b.\r\n" +
                    "200g de Beringelas e/ou cogumelos\r\n" +
                    "Pasta de caril q.b.\r\n" +
                    "Molho de soja q.b.\r\n" +
                    "Sal, pimenta, sumo de lima/limão q.b.\r\n" +
                    "PASTA DE CARIL\r\n" +
                    "Malagueta (opcional) q.b.\r\n" +
                    "4 Dentes de alho (picado)\r\n" +
                    "6 Raiz de coentros (picada)\r\n" +
                    "20g de Galanga/gengibre picada\r\n" +
                    "Folhas de manjericão e coentros picados (frescos) q.b.\r\n" +
                    "50g de Amendoins frescos/tostados sem sal'," +
                    "'Preparar primeiro a pasta de caril. Tostam-se os amendoins, se forem frescos, e moem-se todos os ingredientes num liquidificador (1,2,3) ou almofariz. Fritar esta pasta com um pouco de azeite e, quando esteja um pouco dourada (2 a 3 min), adicionar a carne (previamente temperada com pouco sal, limão/lima e pimenta). Deixar que a carne core bem antes de adicionar os vegetais. Suar bem, para que a água libertada por estes se evapore (os cogumelos e as beringelas libertam muita água, sendo que pode ser uma boa opcção cozinhá-los à parte e juntá-los mais tarde, para que o estufado não fique demasiado aquoso).\r\n" +
                    "Por fim, juntar o leite de coco e rectificar o tempero usando molho de soja e, só mesmo se necessário, sal (não esquecer que o molho de soja é extremamente salgado). Servir quente, acompanhado de arroz pilaf ou ao vapor (branco) e uma boa juliana de vegetais salteada com alho. BOM APETITE!'," +
                    "'Valores nutricionais médios por 100 g: energia: 903 kj / 204 kcal; lípidos: 12 g; hidratos de carbono: 16 g; proteínas: 2 g; ', " + R.drawable.curryreceita + ", 4);",
            "INSERT INTO DetalhesProduto(id_detalhe, ingredientes_produto, nome_receita, ingredientes_receita, texto_receita, valor_nutricional, foto_receita, id_produto) VALUES (5,'Ingredientes: Farinha de arroz 87%, água, fécula de mandioca 3%. '," +
                    "'Massa de arroz agridoce'," +
                    "'4 bifes de vaca\r\n" +
                    "250 g de massa de arroz\r\n" +
                    "200 g de ervilhas\r\n" +
                    "1 cenoura\r\n" +
                    "1 Pimento Vermelho\r\n" +
                    "1 lata pequena de milho\r\n" +
                    "1 lata pequena de rebentos de soja\r\n" +
                    "3 dentes de alho\r\n" +
                    "0,5 dl de azeite\r\n" +
                    "0,5 dl de molho de soja\r\n" +
                    "1 colher (sopa) de açúcar amarelo\r\n" +
                    "Sal e pimenta q.b.'," +
                    "'Arranje e corte os bifes em tiras finas e tempere-as com os dentes de alho picados, sal e pimenta. Descasque e lave a cenoura e corte-a em tiras. Lave o pimento, corte-o ao meio, rejeite-lhe as pevides e peles brancas e corte-o também em tiras finas. Leve a escaldar as ervilhas e a cenoura em água temperada de sal e escorra-as.\r\n" +
                    "Coza a massa de arroz em água temperada de sal conforme as instruções da embalagem, escorra-a, passe-a por água fria e deixe-a escorrer bem. Aqueça o azeite num tacho ao lume, adicione a carne e deixe cozinhar até ficar douradinha. Junte depois o molho de soja e o açúcar amarelo, envolva, adicione o pimento e deixe saltear muito bem.\r\n" +
                    "Junte o milho e os rebentos de soja escorridos, envolva e deixe cozinhar durante mais 1 minuto. Adicione então as ervilhas, a cenoura e a massa, envolva, deixe aquecer muito bem, retire do lume e sirva decorado a gosto. No emprego, aqueça no microondas.'," +
                    "'Valores nutricionais médios por 100g: Não Preparado: Energia: 1479.00 kJ / 349.00 kcal; Lípidos: 0.50 g, Dos quais ácidos gordos saturados: 0.30 g; Hidratos de carbono: 79.00 g; Proteínas: 6.10 g; Sal: 0.01 g ', " + R.drawable.massaarrozreceita + ", 5);",
            "INSERT INTO DetalhesProduto(id_detalhe, ingredientes_produto, nome_receita, ingredientes_receita, texto_receita, valor_nutricional, foto_receita, id_produto) VALUES (6,'Ingredientes: Sumo de tomate de concentrado (água, pasta de tomate), xarope de milho c/alto teor de fructose, xarope de milho, vinagre destilado, melaço, contém 2% ou menos de: sal, farelo de mostarda, fibra de tomate, aromas naturais, especiarias, goma guar, paprica, pectina, goma de feijao carob.'," +
                    "'Leitão com molho barbecue'," +
                    "'1 leitão\r\n" +
                    "2 dentes de alho\r\n" +
                    "1 folha de louro\r\n" +
                    "10 grãos de pimenta do reino preta picada\r\n" +
                    "1 colher de sobremesa de alecrim picado\r\n" +
                    "3 colheres de sobremesa de tomilho picado\r\n" +
                    "Sal grosso – 2 colheres de sobremesa\r\n" +
                    "2 rodelas de limão\r\n" +
                    "1 kg de banha de porco'," +
                    "'Numa panela de fundo grosso, em fogo baixo, derreta a banha e coloque todos os todos os temperos. Depois, posicione a peça de carne com o couro para o lado de cima. A paleta do leitão precisa ficar coberta de banha. Cozinhe em fogo baixo o tempo todo por aproximadamente 1 hora e 20 minutos a 1 hora e 40 minutos.\r\n" +
                    "Para saber se está no ponto, vire cuidadosamente a peça e espete a carne próxima ao osso com uma faca de legumes. Se a carne estiver bem macia, retire cuidadosamente da panela e coloque em uma assadeira. Retire o excesso de gordura e leve ao forno pré aquecido a 250 graus até o couro alourar.'," +
                    "'Valores nutricionais médios por 36g: 60kcal/251kj, gordura 0g, colesterol 0mg, sódio 230mg, potássio 75mg, hidratos de carbono 16g: fibra diet.<1g, açucares 11g, proteinas 0g. ', " + R.drawable.barbecuereceita + ", 6);",
            "INSERT INTO DetalhesProduto(id_detalhe, ingredientes_produto, nome_receita, ingredientes_receita, texto_receita, valor_nutricional, foto_receita, id_produto) VALUES (7,'Ingredientes: Cogumelos desidratados cantharellus cibarius '," +
                    "'Esparguete com cogumelo seco'," +
                    "'60 g de cogumelos desidratados\r\n" +
                    "3 chávenas (chá) de água a ferver\r\n" +
                    "Sal\r\n" +
                    "1 colher (sopa) de azeite (e mais para regar)\r\n" +
                    "1/4 de chávena (chá) de manteiga\r\n" +
                    "1 cebola grande picada\r\n" +
                    "1/2 chávena (chá) de vinho branco seco\r\n" +
                    "500 ml de creme de leite fresco\r\n" +
                    "100 g de parmesão ralado fino (e mais para polvilhar)\r\n" +
                    "500 g de esparguete'," +
                    "'Lave muito bem os cogumelos em água corrente e deixe-os de molho na água a ferver por 30 minutos.\r\n" +
                    "Escorra bem (reserve a água) e, se tiver alguma impureza sedimentada, remova. Corte os cogumelos em tiras. Reserve.\r\n" +
                    "Coloque 5 litros de água com sal para ferver para cozinhar o esparguete.\r\n" +
                    "Aqueça o azeite com a manteiga em fogo baixo e frite a cebola até alourar. Junte as tiras de cogumelo e refogue rapidamente.\r\n" +
                    "Adicione o vinho e espere o álcool evaporar (cerca de 2 minutos). Acrescente a água dos cogumelos e cozinhe por 10 minutos, mexendo de vez em quando, até que o caldo reduza à metade.\r\n" +
                    "Agregue o creme de leite e mexa até ferver. Incorpore o parmesão, ajuste o sal e retire do fogo.\r\n" +
                    "Cozinhe a massa na água a ferver pelo tempo estipulado na embalagem. Escorra, regue com um fio de azeite e sirva com o molho e queijo ralado a gosto.'," +
                    "'Valores nutricionais médios por 100 g: Energia: 76 kJ / 18 kcal; lípidos: 0 g, dos quais saturados: 0 g; hidratos de carbono: 1 g, dos quais açúcares: 0,2 g; fibra: 2,3 g; proteínas: 2,4 g; sal: 0,60 g', " + R.drawable.cogumelosreceita + ", 7);",
            "INSERT INTO DetalhesProduto(id_detalhe, ingredientes_produto, nome_receita, ingredientes_receita, texto_receita, valor_nutricional, foto_receita, id_produto) VALUES (8,'Ingredientes: Carne de caranguejo, água, sal, açúcar'," +
                    "'Esparguete com carne de camarão'," +
                    "'2 dentes de alho\r\n" +
                    "1 colher rasa de sal grosso triturado\r\n" +
                    "1 pimenta grande sem sementes e picada\r\n" +
                    "125g de carne de caranguejo\r\n" +
                    "125ml de azeite extra virgem\r\n" +
                    "Sumo e raspa de 1 limão\r\n" +
                    "500g de esparguete/linguine\r\n" +
                    "Um punhado de salsinha picada\r\n" +
                    "Um punhado de agrião ou rúcula, picados grosseiramente'," +
                    "'Ponha água ao lume para preparar o esparguete.\r\n" +
                    "Enquanto a água está a aquecer, pegue num pilão e deite o alho e o sal em conjunto, e amasse bem para formar uma pasta. Depois coloque a pimenta picada e continue a amassar para que ela também se una à pasta. Coloque esta mistura numa tigela e coloque a carne de caranguejo juntamente, misturando bem. Deite o azeite, a raspa e sumo de limão. Misture tudo bem.\r\n" +
                    "Acabe de preparar o esparguete ou linguine. Escorra e coloque na tigela junto com a carne de caranguejo. Deite a salsinha e agrião ou rúcula. Misture bem. Este prato é uma delícia.'," +
                    "'Valores nutricionais médios por 100 g: Energia: 80 kcal; lípidos: 0.5 g, dos quais saturados: 0 g; hidratos de carbono: 2 g, dos quais açúcares: 2 g; fibra: 0 g; proteínas: 16 g; sal: 0,60 g', " + R.drawable.caranguejoreceita + ", 8);",
    };

    public Database(Context context) {
        super(context, nameDB, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (int i = 0; i < sql.length; i++) {
            db.execSQL(sql[i]);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        version++;
        db.execSQL("DROP TABLE IF EXISTS Utilizador;");
        db.execSQL("DROP TABLE IF EXISTS Produtos;");
        db.execSQL("DROP TABLE IF EXISTS Receitas;");
        this.onCreate(db);
    }

    public long InserirUtilizador(String username, String password) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("username", username);
        cv.put("password", password);
        return db.insert("Utilizador", null, cv);
    }

    public long EditarUtilizador(String username, String password) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("password", password);
        return db.update("Utilizador", cv, "username=?",
                new String[]{username});
    }

    public long EliminarUtilizador(String username) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete("Utilizador", "username=?", new String[]{username});
    }

    public Cursor Select_All_Utilizador() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM Utilizador", null);
    }

    public Cursor Select_Utilizador_By_Username(String username) {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM Utilizador WHERE username=?",
                new String[]{username});
    }

    public int retornaNumUtilizadores()
    {
        int numRows;
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM utilizadores",null);
        numRows = c.getCount();
        return numRows;
    }

    public Cursor Select_All_Produtos() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM Produtos", null);
    }

    public Cursor Select_All_Detalhes() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM DetalhesProduto", null);
    }
    public Cursor Select_NomeByIdProduto(int id_produto) {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT nome_produto FROM Produtos WHERE id_produto=? ", new String[]{String.valueOf(id_produto)});
    }

    public Cursor Select_DetalhesById(int id_produto) {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM DetalhesProduto WHERE id_produto=? ", new String[]{String.valueOf(id_produto)});
    }

    public Cursor Select_PesquisarProduto(String nome_produto, Integer id_categoria) {
        SQLiteDatabase db = getReadableDatabase();

        return db.rawQuery(
                "SELECT * FROM Produtos WHERE lower(nome_produto) like ? " + (
                        id_categoria > 0 ? " AND id_categoria = ?" : ""
                ),
                id_categoria > 0 ?
                        new String[]{"%" + nome_produto + "%", String.valueOf(id_categoria)} :
                        new String[]{"%" + nome_produto + "%"}
        );
    }

    public Cursor Select_All_Categorias() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM Categorias", null);
    }
}
